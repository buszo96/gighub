﻿namespace GigHubApplication.Dtos
{
    public class FollowingDto
    {
        public string FolloweeId { get; set; }
    }
}