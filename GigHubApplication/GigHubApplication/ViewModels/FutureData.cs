﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace GigHubApplication.ViewModels
{
    public class FutureData : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            DateTime result;
            bool isValid = DateTime.TryParseExact(Convert.ToString(value),
                "d MMM yyyy",
                CultureInfo.CurrentCulture,
                DateTimeStyles.None, out result);

            return (isValid && result > DateTime.Now);
        }
    }
}