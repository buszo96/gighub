﻿using System.ComponentModel.DataAnnotations;

namespace GigHubApplication.ViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}