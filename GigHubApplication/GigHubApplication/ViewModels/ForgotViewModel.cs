﻿using System.ComponentModel.DataAnnotations;

namespace GigHubApplication.ViewModels
{
    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}